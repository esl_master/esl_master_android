# ESL Master

The project is to use for adult English learners to improve their knowledge of English. It uses a contextual approach to language learning and also uses documents as entry parameters to help establish the context in which to learn. The project is in the very beginning.
This repository contains the source code for the Android application. For the backend, please refer to https://gitlab.com/mxm_yrmnk/wordfrequency

# Project structure

```

esl_master_android/
│
├── app/
│   ├── src/
│   │   ├── main/
│   │   │   ├── java/
│   │   │   ├── res/
│   │   │   └── AndroidManifest.xml
│   │   └── test/
│   └── build.gradle
│
├── gradle/
│   └── wrapper/
│       ├── gradle-wrapper.jar
│       └── gradle-wrapper.properties
│
├── .gitignore
├── build.gradle
├── gradle.properties
├── gradlew
├── gradlew.bat
├── LICENSE
├── CONTRIBUTING.md
└── README.md



```
## Table of Contents

- [Features](#features)
- [Requirements](#requirements)
- [Installation](#installation)
- [Configuration](#configuration)
- [Usage](#usage)
- [Tests](#tests)

## Features

    TBD

## Requirements

- Android Studio 2022.2.1.19 [https://developer.android.com/docs]
- Java 17.0.1 [https://devdocs.io/openjdk~17/]
- Gradle Groovy 4.0.11 [https://groovy-lang.org/documentation.html]
- Android SDK 12(API level 31, 32), 13(API level 33) [https://developer.android.com/tools/releases/platforms]
- Emulator - Google Pixel 7 Pro, Google Pixel 6a [https://developer.android.com/guide]
- Target Android Version 12, 13

## Installation

1. Clone this repository: `git clone https://gitlab.com/esl_master/esl_master_android.git`
2. Open the project in Android Studio.
3. Set up emulator

## Configuration

- TBD

## Usage

- TBD

## Tests

- TBD


